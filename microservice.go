package main

import (
	"encoding/json"
	"fmt"
	gocql "github.com/gocql/gocql"
	"io/ioutil"
)

type Microservice struct {
	Name string `json:"name"`
	Tu   int    `json:"tu"`
}

func getMicroservice() []Microservice {
	raw, err := ioutil.ReadFile("./pages.json")
	if err != nil {
		fmt.Println(err.Error())
	}

	var c []Microservice
	json.Unmarshal(raw, &c)
	return c
}

type Microservices struct {
	Uid              string         `json:"uid"`
	Type             string         `json:"type"`
	Deployment       string         `json:"deployment"`
	MicroserviceList []Microservice `json:"microservices"`
}

type MetaMicroservice struct {
	UUid       gocql.UUID `json:"uuid"`
	Uid        string     `json:"uid"`
	Type       string     `json:"type"`
	Deployment string     `json:"deployment"`
	Name       string     `json:"name"`
	Tu         int        `json:"tu"`
}

func makeMetaMicroservice(uuid gocql.UUID, uid string, t string, deployment string, name string,
	tu int) MetaMicroservice {
	return MetaMicroservice{
		UUid:       uuid,
		Uid:        uid,
		Type:       t,
		Deployment: deployment,
		Name:       name,
		Tu:         tu,
	}
}
