package main

import (
	_ "flag"
	"fmt"
	gocql "github.com/gocql/gocql"
	"github.com/gorilla/handlers"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	_ "k8s.io/client-go/tools/clientcmd"
	"log"
	"net/http"
	"os"
	_ "path/filepath"
)

/**
 * Main principale del client backend scritto in Go
 */

func main() {
	port := ":" + "9000"
	cassandraPath := os.Getenv("CASSANDRA_HOST")
	if cassandraPath == "" {
		cassandraPath = "127.0.0.1"
	}
	fmt.Println(cassandraPath)
	cluster = gocql.NewCluster(cassandraPath)
	cluster.Keyspace = "service"
	session, _ = cluster.CreateSession()
	defer session.Close()

	/*var kubeconfig *string
	if home := homeDir(); home != "" {
		kubeconfig = flag.String("kubeconfig", filepath.Join(home, ".kube", "config"), "(optional) absolute path to the kubeconfig file")
	} else {
		kubeconfig = flag.String("kubeconfig", "", "absolute path to the kubeconfig file")
	}
	flag.Parse()
	var err error
	// use the current context in kubeconfig
	config, err = clientcmd.BuildConfigFromFlags("", *kubeconfig)
	if err != nil {
		panic(err.Error())
	}*/
	// creates the in-cluster config
	config, err := rest.InClusterConfig()
	if err != nil {
		panic(err.Error())
	}
	if config == nil {
		fmt.Println("config = nil")
	}
	// creates the clientset
	client, err = kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}
	if client == nil {
		fmt.Println("config = nil")
	}

	router := NewRouter()
	log.Fatal(http.ListenAndServe(port, handlers.CORS()(router)))

}

/*
func homeDir() string {
	if h := os.Getenv("HOME"); h != "" {
		return h
	}
	return os.Getenv("USERPROFILE") // windows
}*/
