CREATE KEYSPACE service  WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor' : 1 } ;
use service;
CREATE TABLE service (service_id uuid PRIMARY KEY, uid text, type text, ip text, node text, deployment text, availability double) ;
CREATE INDEX uid ON service (uid);
CREATE INDEX type ON service (type);
CREATE INDEX deployment ON service (deployment);

CREATE TABLE uuid (id int PRIMARY KEY, count counter);
UPDATE uuid SET count = count +0 WHERE id=1;


UPDATE uuid SET count = count +1 WHERE id=1;

CREATE TABLE tu (microservice_id uuid PRIMARY KEY, uid text, type text, microservice text, deployment text, tu bigint);
CREATE INDEX id ON tu (uid);
CREATE INDEX t ON tu (type);
CREATE INDEX dep ON tu (deployment);
CREATE INDEX microservice ON tu (microservice);
