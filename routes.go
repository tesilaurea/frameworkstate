package main

import "net/http"

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

/**
 * Endoint dell'applicazione, con i relativi metodi che vengono invocati
 *
 */

var routes = Routes{
	Route{
		"RegisterService",
		"POST",
		"/register/{service}",
		RegisterService,
	},
	Route{
		"UpdateService",
		"POST",
		"/register/update/{service}",
		UpdateService,
	},
	Route{
		"UpdateAvailability",
		"POST",
		"/availability/update",
		UpdateAvailability,
	},
	Route{
		"UpdateMicroserviceTu",
		"POST",
		"/tu/update",
		UpdateMicroserviceTu,
	},

	Route{
		"Registrated",
		"GET",
		"/registrated/{uid}",
		Registrated,
	},
	Route{
		"GetAllServices",
		"GET",
		"/registrated/services/all",
		GetAllServices,
	},
	Route{
		"GetAllServicesFromType",
		"GET",
		"/registrated/services/ser/{service}",
		GetAllServicesFromType,
	},
	Route{
		"GetAllServicesFromDeployment",
		"GET",
		"/registrated/services/dep/{deployment}",
		GetAllServicesFromDeployment,
	},
	Route{
		"RegisterConfirm",
		"GET",
		"/register/confirm/{deployment}",
		RegisterConfirm,
	},

	Route{
		"RegisterMicroservices",
		"POST",
		"/microservices/register",
		RegisterMicroservices,
	},
	Route{
		"UpdateMicroservices",
		"POST",
		"/microservices/update",
		UpdateMicroservices,
	},
	Route{
		"GetAllMicroservices",
		"GET",
		"/microservices/all",
		GetAllMicroservices,
	},
	Route{
		"GetInfoPodFromMaster",
		"GET",
		"/master/pods/info",
		GetInfoPodFromMaster,
	},
	Route{
		"GetInfoPodFromMasterByType",
		"GET",
		"/master/pods/info/{type}",
		GetInfoPodFromMasterByType,
	},
	Route{
		"PodDetailsFromMaster",
		"GET",
		"/master/pods/details",
		PodDetailsFromMaster,
	},
	Route{
		"NodeDetailsFromMaster",
		"GET",
		"/master/nodes/details",
		NodeDetailsFromMaster,
	},
}
