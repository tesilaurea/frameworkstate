package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	gocql "github.com/gocql/gocql"
	"github.com/gorilla/mux"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"sync"
	_ "time"
)

var mutex sync.Mutex

// Registrazione nuovo service
func RegisterService(w http.ResponseWriter, r *http.Request) {

	//1. ricevo il body dal servizio
	var gocqlUuid gocql.UUID
	var metaPod MetaPod
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	if err = json.Unmarshal(body, &metaPod); err != nil {
		panic(err)
	}
	fmt.Println(metaPod)

	//genero un nuovo uid

	mutex.Lock()
	var i int
	var count int
	counter := session.Query(`SELECT id, count FROM uuid`).Iter()
	for counter.Scan(&i, &count) {
		fmt.Println("Count:", count)
	}
	err = session.Query(`UPDATE uuid SET count = count +1 WHERE id=1`).Exec()
	if err != nil {
		mutex.Unlock()
		panic(err)
	}
	mutex.Unlock()

	//insert
	buf := &bytes.Buffer{}

	gocqlUuid = gocql.TimeUUID()
	trace := gocql.NewTraceWriter(session, buf)
	if err := session.Query(`INSERT INTO service (service_id, uid, type, ip, node, deployment, availability)
		VALUES (?, ?, ?, ?, ?, ?, ?)`, gocqlUuid, strconv.Itoa(count), metaPod.Type, metaPod.Ip, metaPod.Node,
		metaPod.Deployment, 1.0).Trace(trace).Exec(); err != nil {
		log.Fatal("insert:", err)
	} else if buf.Len() == 0 {
		log.Fatal("insert: failed to obtain any tracing")
	}
	buf.Reset()
	metaPod.Uid = strconv.Itoa(count)

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(metaPod); err != nil {
		panic(err)
	}
}

// Aggiornamento Service
func UpdateService(w http.ResponseWriter, r *http.Request) {

	var metaPod MetaPod

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	if err := json.Unmarshal(body, &metaPod); err != nil {
		panic(err)
	}
	fmt.Println(metaPod)

	err = session.Query(`UPDATE service SET ip=?, node=? WHERE service_id=?`,
		metaPod.Ip, metaPod.Node, metaPod.UUid).Exec()
	if err != nil {
		panic(err)
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(metaPod); err != nil {
		panic(err)
	}
}

func UpdateAvailability(w http.ResponseWriter, r *http.Request) {
	var metaPodList []MetaPodAvailability
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	if err := json.Unmarshal(body, &metaPodList); err != nil {
		panic(err)
	}
	fmt.Println(metaPodList)
	for i, _ := range metaPodList {
		err = session.Query(`UPDATE service SET availability=? WHERE service_id=?`,
			metaPodList[i].Availability, metaPodList[i].UUid).Exec()
		if err != nil {
			panic(err)
		}
	}
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
}

func UpdateMicroserviceTu(w http.ResponseWriter, r *http.Request) {

	var microservice_id gocql.UUID
	var uid string
	var microservice string
	var t string
	var tu int64
	var deployment string
	var updateTuTask UpdateTuTask
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	if err := json.Unmarshal(body, &updateTuTask); err != nil {
		panic(err)
	}
	fmt.Println(updateTuTask)

	list := session.Query(`SELECT microservice_id, uid, type, microservice, deployment, tu
							 FROM tu WHERE type=? and deployment=? and microservice=? ALLOW FILTERING`,
		updateTuTask.Type, updateTuTask.Deployment, updateTuTask.Name).Iter()
	for list.Scan(&microservice_id, &uid, &t, &microservice, &deployment, &tu) {
		err = session.Query(`UPDATE tu SET tu=? WHERE microservice_id=?`,
			updateTuTask.Tu, microservice_id).Exec()
		if err != nil {
			//panic(err)
			fmt.Println("Errore nell'aggiornare tu di " +
				uid + " " + t + " " + deployment + " " + microservice)
		}
	}
	if err := list.Close(); err != nil {
		log.Fatal(err)
	}
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
}

func Registrated(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["uid"]
	var service_id gocql.UUID
	var uid string
	var ip string
	var t string
	var node string
	var deployment string
	var availability float64
	var metaPodList []MetaPodAvailability
	list := session.Query(`SELECT service_id, uid, ip, type, node, deployment, availability
							 FROM service WHERE uid=?`, id).Iter()
	for list.Scan(&service_id, &uid, &ip, &t, &node, &deployment, &availability) {
		m := makeMetaPodAvailability(
			makeMetaPod(service_id, uid, t, ip, node, deployment), availability)
		metaPodList = append(metaPodList, m)
		//fmt.Println("Service:", service_id, ip)
	}
	if err := list.Close(); err != nil {
		log.Fatal(err)
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(metaPodList); err != nil {
		panic(err)
	}
}

func GetAllServices(w http.ResponseWriter, r *http.Request) {
	// read all
	var service_id gocql.UUID
	var uid string
	var ip string
	var t string
	var node string
	var deployment string
	var availability float64
	var metaPodList []MetaPodAvailability

	list := session.Query(`SELECT service_id, uid, ip, type, node, deployment, availability
							 FROM service`).Iter()
	for list.Scan(&service_id, &uid, &ip, &t, &node, &deployment, &availability) {
		m := makeMetaPodAvailability(
			makeMetaPod(service_id, uid, t, ip, node, deployment), availability)
		metaPodList = append(metaPodList, m)
		//fmt.Println("Service:", service_id, ip)
	}
	if err := list.Close(); err != nil {
		log.Fatal(err)
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(metaPodList); err != nil {
		panic(err)
	}
}

func GetAllServicesFromType(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	ser := vars["service"]
	var service_id gocql.UUID
	var uid string
	var ip string
	var t string
	var node string
	var deployment string
	var availability float64
	var metaPodList []MetaPodAvailability

	list := session.Query(`SELECT service_id, uid, ip, type, node, deployment, availability
							 FROM service WHERE type=?`, ser).Iter()
	for list.Scan(&service_id, &uid, &ip, &t, &node, &deployment, &availability) {
		m := makeMetaPodAvailability(
			makeMetaPod(service_id, uid, t, ip, node, deployment), availability)
		metaPodList = append(metaPodList, m)
		//fmt.Println("Service:", service_id, ip)
	}
	if err := list.Close(); err != nil {
		log.Fatal(err)
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(metaPodList); err != nil {
		panic(err)
	}
}

func GetAllServicesFromDeployment(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	dep := vars["deployment"]
	var service_id gocql.UUID
	var uid string
	var ip string
	var t string
	var node string
	var deployment string
	var availability float64
	var metaPodList []MetaPodAvailability
	list := session.Query(`SELECT service_id, uid, ip, type, node, deployment, availability
							 FROM service WHERE deployment=?`, dep).Iter()
	for list.Scan(&service_id, &uid, &ip, &t, &node, &deployment, &availability) {
		m := makeMetaPodAvailability(
			makeMetaPod(service_id, uid, t, ip, node, deployment), availability)
		metaPodList = append(metaPodList, m)
		//fmt.Println("Service:", service_id, ip)
	}
	if err := list.Close(); err != nil {
		log.Fatal(err)
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(metaPodList); err != nil {
		panic(err)
	}
}
func RegisterConfirm(w http.ResponseWriter, r *http.Request) {
	// Prendi tutti i servizi dalla tabella, e restituiscili
	vars := mux.Vars(r)
	dep := vars["deployment"]
	// read all
	var service_id gocql.UUID
	var uid string
	var ip string
	var t string
	var node string
	var deployment string
	var metaPodList []MetaPod
	list := session.Query(`SELECT service_id, uid, ip, type, node, deployment
							 FROM service WHERE deployment=?`, dep).Iter()
	for list.Scan(&service_id, &uid, &ip, &t, &node, &deployment) {
		m := makeMetaPod(service_id, uid, t, ip, node, deployment)
		metaPodList = append(metaPodList, m)
		//fmt.Println("Service:", service_id, ip)
	}
	if err := list.Close(); err != nil {
		log.Fatal(err)
	}
	if len(metaPodList) > 1 {
		fmt.Println("Errore Grave!!!")
	}
	if len(metaPodList) == 1 {
		re := makeRegistratedEntity(true, metaPodList[0].UUid, metaPodList[0].Uid,
			metaPodList[0].Type, metaPodList[0].Ip,
			metaPodList[0].Node, metaPodList[0].Deployment)
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusOK)
		if err := json.NewEncoder(w).Encode(re); err != nil {
			panic(err)
		}
		return
	}

	re := makeRegistratedEntity(false, gocql.TimeUUID(), "",
		"", "", "", "")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(re); err != nil {
		panic(err)
	}
}

func RegisterMicroservices(w http.ResponseWriter, r *http.Request) {
	var microservices Microservices

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	if err := json.Unmarshal(body, &microservices); err != nil {
		panic(err)
	}
	fmt.Println(microservices)

	for _, g := range microservices.MicroserviceList {
		//insert
		buf := &bytes.Buffer{}
		gocqlUuid := gocql.TimeUUID()
		trace := gocql.NewTraceWriter(session, buf)
		if err := session.Query(`INSERT INTO tu (microservice_id, uid, type, microservice, deployment, tu)
			VALUES (?, ?, ?, ?, ?, ?)`, gocqlUuid, microservices.Uid, microservices.Type,
			g.Name, microservices.Deployment, g.Tu).Trace(trace).Exec(); err != nil {
			log.Fatal("insert:", err)
		} else if buf.Len() == 0 {
			log.Fatal("insert: failed to obtain any tracing")
		}
		buf.Reset()
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(microservices); err != nil {
		panic(err)
	}

}
func UpdateMicroservices(w http.ResponseWriter, r *http.Request) {
	var microservices Microservices
	var microservice_id gocql.UUID
	var uid string
	var microservice string
	var t string
	var tu int64
	var deployment string
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	if err := json.Unmarshal(body, &microservices); err != nil {
		panic(err)
	}
	fmt.Println(microservices)

	list := session.Query(`SELECT microservice_id, uid, type, microservice, deployment, tu
							 FROM tu WHERE uid=? and deployment=? and type=? ALLOW FILTERING`,
		microservices.Uid, microservices.Deployment, microservices.Type).Iter()
	for list.Scan(&microservice_id, &uid, &t, &microservice, &deployment, &tu) {
		var tu int
		for _, m := range microservices.MicroserviceList {
			if m.Name == microservice {
				tu = m.Tu
			}
		}
		err = session.Query(`UPDATE tu SET tu=? WHERE microservice_id=?`,
			tu, microservice_id).Exec()
		if err != nil {
			panic(err)
		}
	}
	if err := list.Close(); err != nil {
		log.Fatal(err)
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)

}

func GetAllMicroservices(w http.ResponseWriter, r *http.Request) {
	// read all
	var microservice_id gocql.UUID
	var uid string
	var t string
	var microservice string
	var deployment string
	var tu int

	var metaMicroserviceList []MetaMicroservice

	list := session.Query(`SELECT microservice_id, uid, type, microservice, deployment, tu
							 FROM tu`).Iter()
	for list.Scan(&microservice_id, &uid, &t, &microservice, &deployment, &tu) {

		m := makeMetaMicroservice(microservice_id, uid, t, deployment, microservice, tu)
		metaMicroserviceList = append(metaMicroserviceList, m)
		//fmt.Println("Service:", service_id, ip)
	}
	if err := list.Close(); err != nil {
		log.Fatal(err)
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(metaMicroserviceList); err != nil {
		panic(err)
	}
}

func GetInfoPodFromMaster(w http.ResponseWriter, r *http.Request) {
	alivePodsList := PodInCluster()
	var metapodList []AlivePod
	for _, a := range alivePodsList {
		m := makeAlivePod(a.Name, a.PodIP)
		metapodList = append(metapodList, m)
	}
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(metapodList); err != nil {
		panic(err)
	}
}
func GetInfoPodFromMasterByType(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	t := vars["type"]
	alivePodsList := PodInCluster()
	var metapodList []AlivePod
	for _, a := range alivePodsList {
		if a.Name == t {
			m := makeAlivePod(a.Name, a.PodIP)
			metapodList = append(metapodList, m)
		}
	}
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(metapodList); err != nil {
		panic(err)
	}
}

func PodDetailsFromMaster(w http.ResponseWriter, r *http.Request) {

	alivePodsList := PodDetailsOutCluster()

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(alivePodsList); err != nil {
		panic(err)
	}
}

func NodeDetailsFromMaster(w http.ResponseWriter, r *http.Request) {

	nodeList := NodeDetailsOutCluster()

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(nodeList); err != nil {
		panic(err)
	}
}
