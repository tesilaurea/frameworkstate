package main

type UpdateTuTask struct {
	Type       string `json:"type"`
	Deployment string `json:"deployment"`
	Name       string `json:"name"`
	Tu         int    `json:"tu"`
}
