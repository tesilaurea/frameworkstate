package main

import (
	_ "flag"
	"fmt"
	"k8s.io/api/core/v1"
	//"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	//"k8s.io/client-go/kubernetes"
	_ "k8s.io/client-go/rest"
	_ "k8s.io/client-go/tools/clientcmd"
	_ "os"
	_ "path/filepath"
)

type ValueType []string

func PodInCluster() AlivePods {
	/*// creates the in-cluster config
	config, err := rest.InClusterConfig()
	if err != nil {
		panic(err.Error())
		return nil
	}
	// creates the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
		return nil
	}*/

	//multimap := make(map[string]ValueType)
	pods, err := client.CoreV1().Pods("default").List(metav1.ListOptions{})
	if err != nil {
		panic(err.Error())
		return nil

	}

	// create the clientset
	/*	clientset, err := kubernetes.NewForConfig(config)
		if err != nil {
			panic(err.Error())
		}
		pods, err := clientset.CoreV1().Pods("").List(metav1.ListOptions{})
		if err != nil {
			panic(err.Error())
		}
	*/fmt.Printf("There are %d pods in the cluster\n", len(pods.Items))

	//namespace := "default"
	/*pod := "example-xxxxx"
	_, err = client.CoreV1().Pods(namespace).Get(pod, metav1.GetOptions{})
	if errors.IsNotFound(err) {
		fmt.Printf("Pod %s in namespace %s not found\n", pod, namespace)
	} else if statusError, isStatus := err.(*errors.StatusError); isStatus {
		fmt.Printf("Error getting pod %s in namespace %s: %v\n",
			pod, namespace, statusError.ErrStatus.Message)
	} else if err != nil {
		panic(err.Error())
	} else {
		fmt.Printf("Found pod %s in namespace %s\n", pod, namespace)
	}*/
	var alivePodsList AlivePods
	multimap := make(map[string]ValueType)
	for i, _ := range pods.Items {
		fmt.Println(pods.Items[i].GetName())
		name := pods.Items[i].GetName()
		label := ""
		for j, _ := range pods.Items[i].Labels {
			fmt.Println(pods.Items[i].Labels[j])
			if label < pods.Items[i].Labels[j] {
				label = pods.Items[i].Labels[j]
			} else if len(pods.Items[i].Labels) == 1 {
				label = pods.Items[i].Labels[j]
			}
		}
		if label == "" {
			label = name
		}
		fmt.Println(pods.Items[i].Status.PodIP)
		fmt.Println(pods.Items[i].String())
		podIp := pods.Items[i].Status.PodIP
		// First insertion in multimap
		//if pods.Items[i].Status.Phase == "Running" {
		if value, ok := multimap[label]; ok {
			value = append(value, podIp)
			multimap[label] = value
		} else {
			multimap[label] = ValueType{podIp}
		}
		//}
	}
	for key, value := range multimap {
		for _, item := range value {
			fmt.Printf("key, value = %v, %v\n", key, item)
			alivePod := makeAlivePod(key, item)
			alivePodsList = append(alivePodsList, alivePod)
		}
	}

	return alivePodsList
}

type AlivePod struct {
	Name  string `json:"name"`
	PodIP string `json:"podip"`
	// eventuale label per indicare che il pod è un particolare servizio
}
type AlivePods []AlivePod

type PodDetails struct {
	Spec   v1.PodSpec   `json:"podspec"`
	Status v1.PodStatus `json:"podstatus"`
}

type NodeDetails struct {
	Spec   v1.NodeSpec   `json:"nodespec"`
	Status v1.NodeStatus `json:"nodestatus"`
}

func makeAlivePod(name string, podip string) AlivePod {
	return AlivePod{
		Name:  name,
		PodIP: podip,
	}
}

func PodDetailsOutCluster() []PodDetails {
	// create the clientset
	/*clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}*/
	pods, err := client.CoreV1().Pods("default").List(metav1.ListOptions{})
	if err != nil {
		panic(err.Error())
	}
	fmt.Printf("There are %d pods in the cluster\n", len(pods.Items))

	/*namespace := "default"
	pod := "example-xxxxx"
	_, err = clientset.CoreV1().Pods(namespace).Get(pod, metav1.GetOptions{})
	if errors.IsNotFound(err) {
		fmt.Printf("Pod %s in namespace %s not found\n", pod, namespace)
	} else if statusError, isStatus := err.(*errors.StatusError); isStatus {
		fmt.Printf("Error getting pod %s in namespace %s: %v\n",
			pod, namespace, statusError.ErrStatus.Message)
	} else if err != nil {
		panic(err.Error())
	} else {
		fmt.Printf("Found pod %s in namespace %s\n", pod, namespace)
	}*/
	var details []PodDetails
	for i, _ := range pods.Items {
		d := PodDetails{Spec: pods.Items[i].Spec, Status: pods.Items[i].Status}
		details = append(details, d)
	}
	return details
}

func NodeDetailsOutCluster() []NodeDetails {
	//fmt.Printf("Listing nodes in namespace %q:\n", apiv1.NamespaceDefault)
	// create the clientset
	/*clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}*/
	list, err := client.Core().Nodes().List(metav1.ListOptions{})
	if err != nil {
		panic(err)
	}
	var details []NodeDetails
	for i, _ := range list.Items {
		n := NodeDetails{Spec: list.Items[i].Spec, Status: list.Items[i].Status}
		details = append(details, n)
	}
	return details
}
